package com.company.test;

import de.daslaboratorium.machinelearning.classifier.Classifier;
import de.daslaboratorium.machinelearning.classifier.bayes.BayesClassifier;

import java.util.Arrays;

public class SimpleBayesClassification {
    public static void main(String[] args) {
        String[] positiveText1="I love sunny days".split("\\s");
        String[] positiveText2="I love mangos".split("\\s");
        String[] positiveText3="i like umbrella".split("\\s");

        String[] negText1="I hate sunny days".split("\\s");
        String[] negText2="I do not like mangos".split("\\s");
        String[] negText3="i disguise umbrella".split("\\s");
        String[] negText4="these are bad things".split("\\s");

        Classifier<String,String> bayes=new BayesClassifier<String, String>();
        bayes.learn("positive", Arrays.asList(positiveText1));
        bayes.learn("positive", Arrays.asList(positiveText2));
        bayes.learn("positive", Arrays.asList(positiveText3));

        bayes.learn("negative", Arrays.asList(negText1));
        bayes.learn("negative", Arrays.asList(negText2));
        bayes.learn("negative", Arrays.asList(negText3));
        bayes.learn("negative",Arrays.asList(negText4));

        //testing the code

        String[] unknownText1="i am in love with this book".split("\\s");
        String[] unknownText2="they are bad".split("\\s");

        System.out.println(bayes.classify(Arrays.asList(unknownText1)).getCategory());
        System.out.println(bayes.classify(Arrays.asList(unknownText2)).getCategory());
    }
}
